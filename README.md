# WAP technical test

## Goal

The test will be a simple app using React.

Given the list of posts attached, you have to show them on the screen, a user should be able to decide to sort them by tag (alphabetical order) or by number (in data).
A post is composed of a number, text, tag, author.

In the end, the app should look like this:

<img src="picture.png" alt="layout" width="800"/>

## Data

To let you start with real data, posts are available under `src/data`.

## Feature Specs

- Render the list of posts in `src/data` on the screen
- As a user, I want to be able to sort posts by tag
- As a user, I want to be able to sort posts by number
- As a user, I want to be able to post using markdowns

## Technical constraints

- You must use React
- You are free to setup / organize the project as you want (file structure, ...)
- You are free to use any third party dependencies (material UI, lodash ...)
- Add documentation with anything you find relevant
- Adding unit / integration tests is a plus

## Time constraint

We expect you to accomplish as much as you can in less than 3 hours. Of course you will need to prioritize, if you feel you can't implement all of the features (we don't expect you to be able to :stuck_out_tongue: ), please explain in the documentation why you chose to do some features over others.

## Development

Install the bootstrap app with `npm i` and then start to work with `npm start`. You can run the existing test with `npm run test`.
