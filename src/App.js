import React, { useState } from 'react'
import './App.css'

const App = () => {
  return (
    <div className="container">
      <h1>Posts app</h1>
      <div className="flex-row">
        <div className="flex-large">
          <h2>Add post</h2>
        </div>
        <div className="flex-large">
          <h2>View posts</h2>
        </div>
      </div>
    </div>
  )
}

export default App
